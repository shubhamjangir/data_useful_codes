#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


import os
os.getcwd()


# In[3]:


# Associating a single store_id to a patient_id
import pymysql
import pandas as pd

connection = pymysql.connect(host='127.0.0.1',
                             user='read-only',
                             password='read-only',                            
                             db='prod2-generico',
                             port=3307)


q2 = """
select `patient-id`,`store-id`,
count(distinct `id`) as store_bills,
sum(`net-payable`) as store_spend
from `bills-1`
where date(`created-at`)<='2019-10-15'
group by `patient-id`,`store-id`
"""
data_store = pd.read_sql_query(q2, connection)
connection.close()
data_store.columns = [c.replace('-', '_') for c in data_store.columns]


# In[4]:


# Rank them on total visits first and then on total spend - only to associate unique store to each patient
data_store['rank'] = data_store.sort_values(['store_bills','store_spend'], ascending=[False,False])              .groupby(['patient_id'])              .cumcount()+1
data_store.head()


# In[5]:


patient_store = data_store[data_store['rank']==1][['patient_id','store_id']]
patient_store.head()


# In[6]:


# Append store names
import pymysql
import pandas as pd

connection = pymysql.connect(host='127.0.0.1',
                             user='read-only',
                             password='read-only',                            
                             db='prod2-generico',
                             port=3307)


q3 = """
select `id` as store_id,`name` as `store_name` from `stores`
"""
stores = pd.read_sql_query(q3, connection)
connection.close()
stores.columns = [c.replace('-', '_') for c in stores.columns]


# In[7]:


patient_store = patient_store.merge(stores,how='left',on=['store_id','store_id'])
patient_store.head()


# In[8]:


#patient_store.to_csv('/Users/shubhamjangir/Desktop/generico/patient_store_1015.csv')
